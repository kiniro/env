const print =
      x => typeof x.toString == 'function' ? x.toString() : x;

/** Runtime environment for kiniro-lang */
class Env {
    /** @param parent {Env|object} */
    constructor (parent = {}) {
        // map
        this.m = Object.create(parent.m || {});
        // parent environment
        this.__defineSetter__('p', x => { throw 'Env: trying to manually set parent'; });
        this.__defineGetter__('p', () => parent);
    }

    /** Checks whether this Env has parent Env. */
    hasParent () {
        return (this.p instanceof Env);
    }

    /** Defines a new variable.
        @param name {string|symbol} - new variable name
        @param value {any} - new variable value */
    define (name, value) {
        this.m[name] = value;
        return value;
    }

    /** Checks whether a variable is defined within **this** environment.
        It does not check parent environment.
        @param name {string|symbol} - variable name */
    has (name) {
        return this.m.hasOwnProperty(name);
    }

    /** Remove variable from current Env without modifying parent environment.
        @param name {string|symbol} - variable name
     */
    undefine (name) {
        delete this.m[name];
    }

    /** Returns a value by name if it exists, throws otherwise.
        @param name {string|symbol} - variable name */
    lookup (name) {
        if (name in this.m) {
            return this.m[name];
        }

        throw `Couldn't lookup ${print(name)}.`;
    }

    /** Overwrites an existing variable binding with a new value.
        Throws if no variable found.
        @param name {string|symbol} - variable name
        @param value {any} - variable new value */
    set (name, value) {
        if (!(name in this.m)) {
            throw `Variable ${print(name)} is not defined.`;
        }

        let that = this;

        while (true) {
            if (that.has(name)) {
                that.m[name] = value;
                return value;
            } else if (that.hasParent()) {
                that = that.p;
            } else {
                break;
            }
        }
    }

    /** Deletes variable from environment - modifies parent environments if
        needed.
        @param name {string|symbol} - variable name
    */
    unset (name) {
        if (!(name in this.m)) {
            return;
        }

        let that = this;

        while (true) {
            if (that.has(name)) {
                that.undefine(name);
            } else if (that.hasParent()) {
                that = that.p;
            } else {
                break;
            }
        }
    }

    /** Construct a new Env. All variable definitions that will take
        place in it later will not affect the current environment,
        however, variable values can be changed using Env.set(). */
    clone () {
        return new Env(this);
    }
}

module.exports = Env;
