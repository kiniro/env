const assert = require('assert');

const Env = require('../index.js');

describe('Env', () => {
    describe('constructor', () => {
        it('does not throw without parameters', () => {
            assert.doesNotThrow(() => new Env());
        });

        it('does not throw with parameters', () => {
            assert.doesNotThrow(() => new Env(new Env()));
        });

        it('sets parent value correctly', () => {
            const p = new Env();
            assert.deepStrictEqual(new Env(p).p, p);
        });

        it('forbids to set parent explicitly', () => {
            const env = new Env();
            assert.throws(() => env.p = env);
        });
    });

    describe('hasParenth', () => {
        it('returns correct values', () => {
            const env = new Env();
            const env1 = env.clone();
            const env2 = new Env(env1);
            assert.equal(env.hasParent(), false);
            assert.equal(env1.hasParent(), true);
            assert.equal(env2.hasParent(), true);
        });
    });

    describe('lookup', () => {
        it('throws if no variable found', () => {
            assert.throws(() => new Env().lookup('42'), err => err == `Couldn't lookup 42.`);
        });

        it('does not throw if variable found', () => {
            assert.doesNotThrow(() => {
                const env = new Env();
                env.define('42', 42);
                env.lookup('42');
            });
        });

        it('returns correct value', () => {
            const env = new Env();
            env.define('42', 42);
            assert.deepStrictEqual(env.lookup('42'), 42);
        });
    });

    describe('define', () => {
        it('defines new variable', () => {
            const env = new Env();
            env.define('x', 1);
            assert.deepStrictEqual(env.lookup('x'), 1);
        });

        it('does not throw if variable exists', () => {
            const env = new Env();
            env.define('x', 1);
            assert.doesNotThrow(() => env.define('x', 2));
        });

        it('overwrites variables', () => {
            const env = new Env();
            env.define('x', 1);
            env.define('x', 2);
            assert.deepStrictEqual(env.lookup('x'), 2);
        });

        it('does no overwrite variables defined in parent Env', () => {
            const env1 = new Env();
            env1.define('x', 1);
            const env2 = new Env(env1);
            env2.define('x', 2);
            assert.deepStrictEqual(env1.lookup('x'), 1);
            assert.deepStrictEqual(env2.lookup('x'), 2);
        });
    });

    describe('set', () => {
        it('throws if no variable found', () => {
            const env = new Env();
            assert.throws(() => env.set('x', 0), err => err == 'Variable x is not defined.');
        });

        it('modifies variable', () => {
            const env = new Env();
            env.define('x',0);
            env.set('x', 1);
            assert.deepStrictEqual(env.lookup('x'), 1);
        });

        it('modifies variable defined in parent Env', () => {
            const env1 = new Env();
            env1.define('x', 0);
            const env = new Env(env1);
            env.set('x', 1);
            assert.deepStrictEqual(env.lookup('x'), 1);
        });


        it('modifies variable defined in parent Env (2)', () => {
            const env1 = new Env();
            env1.define('x', 0);
            const env2 = new Env(env1);
            const env = new Env(env2);
            env.set('x', 1);
            assert.deepStrictEqual(env.lookup('x'), 1);
            assert.deepStrictEqual(env1.lookup('x'), 1);
            assert.deepStrictEqual(env2.lookup('x'), 1);
        });


        it('modifies variable defined in parent Env (2)', () => {
            const env1 = new Env();
            env1.define('x', 1);
            const env2 = new Env(env1);
            const env3 = new Env(env2);
            env2.define('x', 2);
            env3.set('x', 12);
            env3.define('x', 3);
            env3.set('x', 13);
            assert.deepStrictEqual(env1.lookup('x'), 1);
            assert.deepStrictEqual(env2.lookup('x'), 12);
            assert.deepStrictEqual(env3.lookup('x'), 13);
        });
    });

    describe('unset', () => {
        it('should not throw if no variable is defined', () => {
            const env = new Env();
            assert.doesNotThrow(() => env.unset('x'));
        });

        it('should unset variable defined in the same env', () => {
            const env = new Env();
            env.define('x', 1);
            env.unset('x');
            assert.equal(env.has('x'), false);
        });

        it('should unset variable defined in the parent env', () => {
            const env1 = new Env();
            const env2 = env1.clone();
            const env3 = env2.clone();
            const env4 = env3.clone();
            env1.define('x', 1);
            env4.unset('x');
            assert.equal(env1.has('x'), false);
            assert.equal(env2.has('x'), false);
            assert.equal(env3.has('x'), false);
            assert.equal(env4.has('x'), false);
        });
    });

    describe('undefine', () => {
        it('does not throw if variable is non-existant', () => {
            assert.doesNotThrow(() => new Env().undefine('x'));
        });

        it('removes variable from environment', () => {
            const env = new Env();
            env.define('x', 1);
            env.undefine('x');
            assert.equal(env.has('x'), false);
        });

        it('does not remove variable from parent environment', () => {
            const env1 = new Env();
            env1.define('x', 1);
            const env2 = env1.clone();
            env2.undefine('x');
            assert.equal(env1.has('x'), true);
            assert.equal(env2.has('x'), false);
        });

        it('does not remove variable from parent environment', () => {
            const env1 = new Env();
            env1.define('x', 1);
            const env2 = env1.clone();
            env2.undefine('x');
            assert.equal(env1.has('x'), true);
            assert.equal(env2.has('x'), false);
        });

        it('does not remove variable from parent environment', () => {
            const env1 = new Env();
            env1.define('x', 1);
            const env2 = env1.clone();
            env2.define('x', 2);
            env2.undefine('x');
            assert.equal(env1.has('x'), true);
            assert.equal(env2.has('x'), false);
        });
    });

    describe('has', () => {
        it('returns false if variable is not defined', () => {
            assert.deepStrictEqual(new Env().has('x'), false);
        });

        it('returns true if variable is defined', () => {
            const env = new Env();
            env.define('x', 1);
            assert.deepStrictEqual(env.has('x'), true);
        });

        it('returns false if variable is defined in parent', () => {
            const env1 = new Env();
            env1.define('x', 1);
            const env2 = new Env(env1);
            assert.deepStrictEqual(env2.has('x'), false);
        });

        it('returns false if variable is defined in parent', () => {
            const env1 = new Env();
            const env2 = new Env(env1);
            env1.define('x', 1);
            assert.deepStrictEqual(env2.has('x'), false);
            assert.deepStrictEqual(env1.has('x'), true);
        });
    });

    describe('clone', () => {
        it('sets parent', () => {
            const env1 = new Env();
            const env2 = env1.clone();
            assert.deepStrictEqual(env2.p, env1);
        });
    });
});
