![npm bundle size (minified)](https://img.shields.io/bundlephobia/min/@kiniro/env.svg)
![NpmLicense](https://img.shields.io/npm/l/@kiniro/env.svg)

Reusable environment class for [@kiniro/lang](https://gitlab.com/kiniro/lang).

[gitlab](https://gitlab.com/kiniro/env) / [npm](https://www.npmjs.com/package/@kiniro/env) / [jsdoc](https://kiniro.gitlab.io/env)
